import styled from "styled-components";

export const HomePageCardLayout = styled.div`
  background-color: rgba(0, 0, 0, 0.5);
  width: 777px;
  height: 455px;
  left: 331px;
  top: 270px;
  border-radius: 40px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`;

export const IconsContainer = styled.div`
  width: 100%;
  display: flex;
`;

export const KnowMore = styled.button`
  background-color: #d6ffb7;
  color: black;
  font-size: 23px;
  width: 195px;
  height: 55px;
  border-radius: 45px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

