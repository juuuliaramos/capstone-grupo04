//STYLE
import { Container, Card, CardImage, CardBody } from "./style";

const CardItem = () => {
  return (
    <Container>
      <Card>
        <CardImage />
        <CardBody>
          <h2>Lorem Impsun</h2>
          <span>
            Lorem Ipsum é simplesmente uma simulação de texto da indústria
            tipográfica e de impressos, e vem sendo utilizado desde o século
            XVI.
          </span>
        </CardBody>
      </Card>

      <Card>
        <CardImage />
        <CardBody>
          <h2>Lorem Impsun</h2>
          <span>
            Lorem Ipsum é simplesmente uma simulação de texto da indústria
            tipográfica e de impressos, e vem sendo utilizado desde o século
            XVI.
          </span>
        </CardBody>
      </Card>

      <Card>
        <CardImage />
        <CardBody>
          <h2>Lorem Impsun</h2>
          <span>
            Lorem Ipsum é simplesmente uma simulação de texto da indústria
            tipográfica e de impressos, e vem sendo utilizado desde o século
            XVI.
          </span>
        </CardBody>
      </Card>

      <Card>
        <CardImage />
        <CardBody>
          <h2>Lorem Impsun</h2>
          <span>
            Lorem Ipsum é simplesmente uma simulação de texto da indústria
            tipográfica e de impressos, e vem sendo utilizado desde o século
            XVI.
          </span>
        </CardBody>
      </Card>
    </Container>
  );
};
export default CardItem;
