import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  margin: 0 auto;
  margin-top: 101px;
  margin-bottom: 50px;
  padding: 20px;
  width: 979px;
  overflow-x: hidden;

  background: #ffffff;
  box-shadow: 2px 4px 4px rgba(0, 0, 0, 0.25);
`;
